﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

using Caliburn.Micro;

using ToDoManager.Input;
using ToDoManager.UserInterface.ViewModels;
using ToDoManager.UserInterface.ViewModels.Frames;
using ToDoManager.Utils;

namespace ToDoManager
{
    public class AppBootstrapper : BootstrapperBase
    {
        private SimpleContainer container;

        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void BuildUp(object instance)
        {
            container.BuildUp(instance);
        }

        protected override void Configure()
        {
            container = new SimpleContainer();
            container.Instance(container);
            container
                .Singleton<IWindowManager, WindowManager>()
                .Singleton<IEventAggregator, EventAggregator>()
                .Singleton<IDataService, DataService>();

            container
                .PerRequest<ShellViewModel>()
                .PerRequest<GroupListViewModel>()
                .PerRequest<TaskListViewModel>();

            Parser.CreateTrigger = (target, triggerText) => 
            {
                if (triggerText == null)
                {
                    return ConventionManager.GetElementConvention(target.GetType()).CreateTrigger();
                }

                string eventName = triggerText
                    .Replace("[", String.Empty)
                    .Replace("]", String.Empty).Trim();

                if (eventName.StartsWith("Key", StringComparison.OrdinalIgnoreCase))
                {
                    eventName = eventName.Replace("Key", String.Empty).Trim(); // key name
                    var key = (Key) Enum.Parse(typeof(Key), eventName, true);
                    return new KeyTrigger() { Key = key };
                }
                
                eventName = eventName.Replace("Event", String.Empty).Trim(); // event name

                return new System.Windows.Interactivity.EventTrigger { EventName = eventName };
            };
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return container.GetAllInstances(service);
        }

        protected override object GetInstance(Type service, string key)
        {
            return container.GetInstance(service, key);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }
    }
}
