﻿using Newtonsoft.Json;

namespace ToDoManager.Models.Data
{
    [JsonObject(MemberSerialization = MemberSerialization.OptOut, IsReference = true)]
    public class Group
    {
        public string Name { get; set; }

        public Group(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return "Group { Name = " + Name + " }";
        }
    }
}
