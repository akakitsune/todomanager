﻿using System;
using System.ComponentModel;

using Newtonsoft.Json;

namespace ToDoManager.Models.Data
{
    [JsonObject(MemberSerialization = MemberSerialization.OptOut)]
    public class Task
    {
        public string Title { get; set; }

        private string description = String.Empty;

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue("")]
        public string Description
        {
            get => description;
            set => description = value ?? String.Empty;
        }

        [DefaultValue(false)]
        public bool IsDone { get; set; }

        [JsonProperty(IsReference = true)]
        public Group Group { get; set; }

        public Task(string title, string description, Group group, bool isDone = false)
        {
            Title = title;
            Description = description;
            Group = group;
            IsDone = isDone;
        }

        public override string ToString()
        {
            return "Task { Title = " + Title + ", " +
                "Description = " + Description + ", " +
                "IsDone = " + (IsDone ? "True" : "False")+ ", " +
                "Group = " + Group.Name + " }";
        }
    }
}
