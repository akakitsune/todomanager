﻿namespace ToDoManager.Models
{
    public class CustomMessage
    {
        public MessageType Type { get; set; }
        public object Message { get; set; }
    }

    public enum MessageType
    {
        UPDATE,
        DELETE
    }
}
