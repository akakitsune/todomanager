﻿using System;
using System.Collections.Generic;

using ToDoManager.Models.Data;

namespace ToDoManager.Utils
{
    public interface IDataService
    {
        void GetData(Action<List<Group>, Exception> callback);
        void GetData(Action<List<Task>, Exception> callback);
        void Add(Group item);
        void Add(Task item);
        void Update(Group oldGroup, Group newGroup);
        void Update(Task oldTask, Task newTask);
        void Remove(Group item);
        void Remove(Task item);
        void Save();
    }
}
