﻿using System;
using System.Collections.Generic;

using ToDoManager.Models.Data;

namespace ToDoManager.Utils
{
    public class DataService : IDataService
    {
        private JsonHelper.RootObject root;

        public DataService()
        {
            root = JsonHelper.Deserialize();
        }

        public void GetData(Action<List<Group>, Exception> callback)
        {
            callback(root.Groups, null);
        }

        public void GetData(Action<List<Task>, Exception> callback)
        {
            callback(root.Tasks, null);
        }

        public void Add(Group item)
        {
            root.Groups.Add(item);
            Save();
        }

        public void Add(Task item)
        {
            root.Tasks.Add(item);
            Save();
        }

        public void Update(Task oldTask, Task newTask)
        {
            oldTask.Title = newTask.Title;
            oldTask.Description = newTask.Description;
            oldTask.IsDone = newTask.IsDone;
            oldTask.Group = newTask.Group;

            Save();
        }

        public void Update(Group oldGroup, Group newGroup)
        {
            oldGroup.Name = newGroup.Name;

            Save();
        }

        public void Remove(Group item)
        {
            root.Groups.Remove(item);
            root.Tasks.RemoveAll(i => i.Group == item);
            Save();
        }

        public void Remove(Task item)
        {
            root.Tasks.Remove(item);
            Save();
        }

        public void Save()
        {
            JsonHelper.Serialize(root);
        }
    }
}
