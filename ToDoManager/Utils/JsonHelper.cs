﻿using System;
using System.Collections.Generic;
using System.IO;

using Newtonsoft.Json;

using ToDoManager.Models.Data;

namespace ToDoManager.Utils
{
    public static class JsonHelper
    {
        public readonly static string JsonFilename = Resources.Strings.ToDoManagerDataFile;

        public class RootObject
        {
            public List<Group> Groups;
            public List<Task> Tasks;
        }

        public static RootObject Deserialize()
        {
            try
            {
                string json = File.ReadAllText(JsonFilename);
                return JsonConvert.DeserializeObject<RootObject>(json);
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return new RootObject() { Groups = new List<Group>(), Tasks = new List<Task>()};
        }

        public static void Serialize(RootObject root)
        {
            string json = JsonConvert.SerializeObject(root, Formatting.Indented);
            File.WriteAllText(JsonFilename, json);
        }
    }
}
