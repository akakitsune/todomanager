﻿using Caliburn.Micro;

using ToDoManager.UserInterface.ViewModels.Frames;

namespace ToDoManager.UserInterface.ViewModels
{
    public class ShellViewModel : PropertyChangedBase
    {
        public string Title => Resources.Strings.AppName;

        public GroupListViewModel GroupsViewModel { get; set; }
        public TaskListViewModel TasksViewModel { get; set; }

        public ShellViewModel(GroupListViewModel groupsViewModel, TaskListViewModel tasksViewModel)
        {
            GroupsViewModel = groupsViewModel;
            TasksViewModel = tasksViewModel;
        }
    }
}
