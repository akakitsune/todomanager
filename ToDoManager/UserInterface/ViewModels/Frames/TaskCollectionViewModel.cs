﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

using Caliburn.Micro;

using ToDoManager.Models;
using ToDoManager.Models.Data;
using ToDoManager.UserInterface.ViewModels.Dialog;
using ToDoManager.UserInterface.ViewModels.Templates;
using ToDoManager.Utils;

namespace ToDoManager.UserInterface.ViewModels.Frames
{
    public class TaskCollectionViewModel : CollectionViewModelBase<TaskViewModel, Task>, IHandle<CustomMessage>
    {
        protected readonly EventAggregator events;

        #region Properties
        private ObservableCollection<TaskViewModel> tasksByGroup;
        public ObservableCollection<TaskViewModel> TasksByGroup
        {
            get => tasksByGroup;
            set
            {
                tasksByGroup = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TasksByGroup"));
            }
        }

        private string newTaskTitle;
        public string NewTaskTitle
        {
            get => newTaskTitle;
            set
            {
                newTaskTitle = value;
                OnPropertyChanged(new PropertyChangedEventArgs("NewTaskTitle"));
            }
        }

        public TaskViewModel SelectedItem
        {
            get => selectedItem;
            set
            {
                selectedItem = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SelectedItem"));
            }
        }

        private Group selectedGroup;
        public Group SelectedGroup
        {
            get => selectedGroup;
            set
            {
                selectedGroup = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SelectedGroup"));
            }
        }

        private bool isDoneShown = false;
        public bool IsDoneShown
        {
            get => isDoneShown;
            set
            {
                isDoneShown = value;
                OnPropertyChanged(new PropertyChangedEventArgs("IsDoneShown"));
            }
        }

        private string isDoneShownContent = Resources.Strings.ShowDoneTasks;
        public string IsDoneShownContent {
            get => isDoneShownContent;
            set
            {
                isDoneShownContent = value;
                OnPropertyChanged(new PropertyChangedEventArgs("IsDoneShownContent"));
            }
        }
        #endregion

        public TaskCollectionViewModel(EventAggregator events) : base()
        {
            this.events = events;
            this.events.Subscribe(this);
            Init();
        }

        #region Commands
        public void KeyDown(KeyEventArgs eventArgs)
        {
            switch (eventArgs.Key)
            {
                case Key.Enter:
                    Create(NewTaskTitle);
                    NewTaskTitle = string.Empty;
                    break;
                case Key.Delete:
                    Remove(SelectedItem);
                    break;
            }
        }

        public void Create(string title)
        {
            if (string.IsNullOrEmpty(title)) return;
            if (string.IsNullOrWhiteSpace(title)) return;
            title = title.Trim();
            title = (new System.Text.RegularExpressions.Regex("\\s+")).Replace(title, " ");
            Task task = new Task(title, SelectedGroup);
            TaskViewModel item = new TaskViewModel(task);
            Add(item);
        }

        // TODO: что-то не так со скрытие задач
        public void SwitchVis()
        {
            if (IsDoneShown)
            {
                IsDoneShown = false;
            } else
            {
                IsDoneShown = true;
            }

            Update();
        }

        public void OpenDialog(object param)
        {
            if (param != null)
            {
                EditDialogViewModel dialog = new EditDialogViewModel(param as TaskViewModel);
                WindowManager manager = new WindowManager();
                manager.ShowDialog(dialog);
                if (dialog.IsAccepted)
                {
                    Update();                    
                }
            }
        }
        #endregion

        #region IHandle
        public void Handle(CustomMessage message)
        {
            switch(message.Type)
            {
                case MessageType.UPDATE:
                    SelectedGroup = message.Message as Group;
                    Update();
                    break;
                case MessageType.DELETE:
                    var data = Collection.Where(i => i.TaskGroup == SelectedGroup).Select(i => i);
                    foreach(TaskViewModel it in data)
                    {
                        it.PropertyChanged -= new PropertyChangedEventHandler(OnItemChanged);
                        Remove(it);
                    }
                    
                    break;
            }
        }
        #endregion

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnCollectionChanged(e);
            Update();
        }

        protected void OnItemChanged(object sender, PropertyChangedEventArgs e)
        {
            System.Console.WriteLine("Property {0}", e.PropertyName);
            if (e.PropertyName == "IsDone") Update();
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsDoneShown")
            {
                if (IsDoneShown)
                {
                    IsDoneShownContent = Resources.Strings.HideDonwTasks;
                }
                else
                {
                    IsDoneShownContent = Resources.Strings.ShowDoneTasks;
                }
            }

            base.OnPropertyChanged(e);
        }

        private void Init()
        {
            TasksByGroup = new ObservableCollection<TaskViewModel>();
            List<Task> data = DataService.Instance.TaskData;
            foreach (Task it in data)
            {
                TaskViewModel taskVM = new TaskViewModel(it);
                taskVM.PropertyChanged += new PropertyChangedEventHandler(OnItemChanged);
                Add(taskVM);
            }
        }

        private void Update()
        {
            TasksByGroup.Clear();
            if (SelectedGroup == null)
            {
                SelectedGroup = DataService.Instance.GroupData[0];
            }

            if (IsDoneShown)
            {
                var tasks = Collection.Where(i => i.TaskGroup == SelectedGroup)
                                      .OrderBy(i => i.IsDone)
                                      .Select(i => i);
                TasksByGroup = new ObservableCollection<TaskViewModel>(tasks);
            }
            else
            {
                var tasks = Collection.Where(i => i.TaskGroup == SelectedGroup && i.IsDone == false)
                                      .Select(i => i);
                TasksByGroup = new ObservableCollection<TaskViewModel>(tasks);
            }
        }
    }
}
