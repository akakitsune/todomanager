﻿using System;
using System.ComponentModel;
using System.Linq;

using Caliburn.Micro;

using ToDoManager.Models.Data;
using ToDoManager.UserInterface.ViewModels.Dialog;
using ToDoManager.UserInterface.ViewModels.Templates;
using ToDoManager.Utils;

namespace ToDoManager.UserInterface.ViewModels.Frames
{
    public class TaskListViewModel : Conductor<TaskViewModel>.Collection.OneActive, IHandle<Group>
    {
        #region Fields
        private readonly IWindowManager manager;
        private readonly IEventAggregator events;
        private readonly IDataService dataService;

        private Group selectedGroup = null;
        private string newTask;
        #endregion

        #region Properties
        public Group SelectedGroup
        {
            get => selectedGroup;
            private set
            {
                selectedGroup = value;
                NotifyOfPropertyChange(() => SelectedGroup);
                NotifyOfPropertyChange(() => IsAddTaskEnabled);
                UpdateViewModel(selectedGroup);
            }
        }

        public string NewTask
        {
            get => newTask;
            set
            {
                newTask = value;
                NotifyOfPropertyChange(() => NewTask);
            }
        }

        public bool IsAddTaskEnabled => selectedGroup != null;
        #endregion

        #region Constructor
        public TaskListViewModel(IWindowManager manager, IEventAggregator events, IDataService dataService)
        {
            this.events = events;
            this.events.Subscribe(this);
            this.manager = manager;
            this.dataService = dataService;

            dataService.GetData((gs, e) => { SelectedGroup = gs.FirstOrDefault(); });
        }
        #endregion

        #region CRUD
        public void Add(string taskTitle)
        {
            if (!string.IsNullOrEmpty(taskTitle) && !string.IsNullOrWhiteSpace(taskTitle))
            {
                Task task = new Task(taskTitle.Trim(), null, selectedGroup);
                Add(new TaskViewModel(task));
            }

            NewTask = String.Empty;
        }

        public void Add(TaskViewModel vm)
        {
            dataService.Add(vm.Data);
            ActivateItem(vm);
        }

        public void Edit(TaskViewModel vm)
        {
            UpdateItem(vm, OpenEditDialog(vm.Data));
        }

        public void Remove(TaskViewModel vm)
        {
            dataService.Remove(vm.Data);
            DeactivateItem(vm, true);
        }
        #endregion

        public override void DeactivateItem(TaskViewModel item, bool close)
        {
            item.PropertyChanged -= OnItemChanged;
            base.DeactivateItem(item, close);
        }

        protected void OnItemChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsTaskDone") // TODO: save if changed
            {
                ActivateItem(sender as TaskViewModel);
                dataService.Save();
            }
        }

        #region IHandle
        public void Handle(Group message)
        {
            SelectedGroup = message;
        }
        #endregion

        #region Private Methods
        private void UpdateViewModel(Group group)
        {
            Items.Clear();
            dataService.GetData((ts, e) => 
            {
                Items.AddRange(ts.Where(t => t.Group == group)
                    .Select(t => 
                    {
                        TaskViewModel task = new TaskViewModel(t);
                        task.PropertyChanged += OnItemChanged;
                        return task;
                    }
                ));
            });

            ActiveItem = Items.FirstOrDefault();
        }

        private void UpdateItem(TaskViewModel item, Task newData)
        {
            if (newData == null) return;

            dataService.Update(item.Data, newData);

            item.TaskTitle = newData.Title;
            item.TaskDescription = newData.Description;
            item.IsTaskDone = newData.IsDone;
            item.TaskGroup = newData.Group;
        }

        private Task OpenEditDialog(Task data)
        {
            EditModalDialogViewModel<Task> dialog = new EditModalDialogViewModel<Task>(data);
            bool? result = manager.ShowDialog(dialog);
            if (result != null && result == true)
            {
                return dialog.Data;
            }

            return null;
        }
        #endregion
    }
}
