﻿using System.ComponentModel;
using System.Linq;

using Caliburn.Micro;

using ToDoManager.Models.Data;
using ToDoManager.UserInterface.ViewModels.Dialog;
using ToDoManager.UserInterface.ViewModels.Templates;
using ToDoManager.Utils;

namespace ToDoManager.UserInterface.ViewModels.Frames
{
    public class GroupListViewModel : Conductor<GroupViewModel>.Collection.OneActive
    {
        private readonly IWindowManager manager;
        private readonly IEventAggregator events;
        private readonly IDataService dataService;

        public GroupListViewModel(IWindowManager manager, IEventAggregator events, IDataService dataService)
        {
            this.manager = manager;
            this.events = events;
            this.dataService = dataService;

            this.dataService.GetData((gs, e) => 
                Items.AddRange(gs.Select(g =>
                {
                    GroupViewModel vm = new GroupViewModel(g);
                    vm.PropertyChanged += OnItemChanged;
                    return vm;
                })));

            ActiveItem = Items.FirstOrDefault();
        }

        #region CRUD
        public void Add(GroupViewModel vm)
        {
            if (vm == null)
            {
                string defaultName = Resources.Strings.DefaultGroupName;
                vm = new GroupViewModel(new Group(defaultName));
            }

            ActivateItem(vm);
            dataService.Add(vm.Data);
        }

        public void Edit(GroupViewModel vm)
        {
            UpdateItem(vm, OpenEditDialog(vm.Data));
        }

        public void Remove(GroupViewModel vm)
        {
            dataService.Remove(vm.Data);
            DeactivateItem(vm, true);
        }
        #endregion

        protected override void OnActivationProcessed(GroupViewModel item, bool success)
        {
            if (item != null) events.PublishOnUIThread(item.Data);
            base.OnActivationProcessed(item, success);
        }

        public override void DeactivateItem(GroupViewModel item, bool close)
        {
            item.PropertyChanged -= OnItemChanged;
            base.DeactivateItem(item, close);
        }

        // уже ничего не делает
        protected void OnItemChanged(object sender, PropertyChangedEventArgs e) { }

        #region Private Methods
        private void UpdateItem(GroupViewModel item, Group newData)
        {
            if (newData == null) return;

            dataService.Update(item.Data, newData);

            item.GroupName = newData.Name;
        }

        private Group OpenEditDialog(Group data)
        {
            EditModalDialogViewModel<Group> dialog = new EditModalDialogViewModel<Group>(data);
            bool? result = manager.ShowDialog(dialog);
            if (result != null && result == true)
            {
                return dialog.Data;
            }

            return null;
        }
        #endregion
    }
}
