﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;

using Caliburn.Micro;

using ToDoManager.UserInterface.ViewModels.Templates;
using ToDoManager.Utils;
using ToDoManager.Models;
using ToDoManager.Models.Data;

namespace ToDoManager.UserInterface.ViewModels.Frames
{
    public class GroupCollectionViewModel : CollectionViewModelBase<GroupViewModel, Group>
    {
        private EventAggregator events;

        public GroupViewModel SelectedItem
        {
            get => selectedItem;
            set
            {
                if (selectedItem != value)
                {
                    selectedItem = value;
                    events.PublishOnUIThread(new CustomMessage() { Type = MessageType.UPDATE, Message = selectedItem.Data });
                    OnPropertyChanged(new PropertyChangedEventArgs("SelectedItem"));
                }
            }
        }

        public GroupCollectionViewModel(EventAggregator events) : base()
        {
            this.events = events;
            Init();
        }

        #region Commands
        public void Create()
        {
            GroupViewModel item = new GroupViewModel(new Group(Group.NewList));
            Add(item);
        }

        public void StartEdit() => SelectedItem.StartEdit();

        // TODO: show dialog: ask if delete
        public void KeyDown(KeyEventArgs eventArgs)
        {
            switch (eventArgs.Key)
            {
                case Key.Delete:
                    if (SelectedItem.IsRemovable)
                    {
                        int index = IndexOf(SelectedItem);
                        if (index > 0) index--;
                        GroupViewModel old = SelectedItem;
                        SelectedItem = this[index];
                        Remove(old);
                        events.PublishOnUIThread(new CustomMessage() { Type = MessageType.DELETE, Message = old.Data });
                    }

                    break;
            }
        }
        #endregion

        private void Init()
        {
            List<Group> data = DataService.Instance.GroupData;
            foreach (Group it in data)
            {
                Add(new GroupViewModel(it));
            }

            this[0].IsRemovable = false;
            SelectedItem = Collection[0];
        }
    }
}
