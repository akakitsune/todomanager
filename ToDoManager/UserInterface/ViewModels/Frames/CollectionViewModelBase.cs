﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

using ToDoManager.UserInterface.ViewModels.Templates;

namespace ToDoManager.UserInterface.ViewModels.Frames
{
    public abstract class CollectionViewModelBase<TItemViewModel, TItem> 
        : ObservableCollection<TItemViewModel> where TItemViewModel : ItemViewModelBase<TItem>
    {
        protected TItemViewModel selectedItem;

        public ObservableCollection<TItemViewModel> Collection => this;

        public CollectionViewModelBase() 
            : base(new ObservableCollection<TItemViewModel>()) { }

        public CollectionViewModelBase(ObservableCollection<TItemViewModel> collection) 
            : base(collection) { }

        public List<TItem> GetData()
        {
            List<TItem> data = new List<TItem>();
            foreach(TItemViewModel it in Collection)
            {
                data.Add(it.Data);
            }

            return data;
        }
    }
}
