﻿using ToDoManager.Models.Data;
using ToDoManager.UserInterface.ViewModels.Templates;

namespace ToDoManager.UserInterface.ViewModels.Dialog
{
    public class EditModalDialogViewModel<T> : ModalDialogViewModel
    {
        public T Data {
            get
            {
                System.Console.WriteLine((Content as IData<T>).Data);
                return (Content as IData<T>).Data;
            }
        }

        private object content;
        public object Content
        {
            get => content;
            set
            {
                content = value;
                NotifyOfPropertyChange(() => Content);
            }
        }

        // TODO: диалоги бы наверное через Factory
        public EditModalDialogViewModel(object data)
        {
            if (data is Task)
            {
                Task t = data as Task;
                Content = new TaskViewModel()
                {
                    TaskTitle = t.Title,
                    TaskDescription = t.Description,
                    IsTaskDone = t.IsDone,
                    TaskGroup = t.Group
                };
            }
            else if(data is Group)
            {
                Group g = data as Group;
                Content = new GroupViewModel() { GroupName = g.Name };
            }
        }
    }
}
