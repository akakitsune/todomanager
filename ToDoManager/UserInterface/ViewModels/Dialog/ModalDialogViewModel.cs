﻿using Caliburn.Micro;

namespace ToDoManager.UserInterface.ViewModels.Dialog
{
    public class ModalDialogViewModel : Screen, IDialog
    {
        private bool? dialogResult = null;
        public bool? DialogResult
        {
            get => dialogResult;
            set => dialogResult = value;
        }

        public ModalDialogViewModel() { }

        public virtual void Accept()
        {
            dialogResult = true;
            TryClose(dialogResult);
        }
    }
}
