﻿namespace ToDoManager.UserInterface.ViewModels.Dialog
{
    public interface IDialog
    {
        bool? DialogResult { get; }
    }
}
