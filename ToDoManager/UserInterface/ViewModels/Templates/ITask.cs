﻿using ToDoManager.Models.Data;

namespace ToDoManager.UserInterface.ViewModels.Templates
{
    public interface ITask
    {
        string TaskTitle { get; set; }
        string TaskDescription { get; set; }
        bool IsTaskDone { get; set; }
        Group TaskGroup { get; set; }
    }
}
