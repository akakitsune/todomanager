﻿using System;

using Caliburn.Micro;

using ToDoManager.Models.Data;

namespace ToDoManager.UserInterface.ViewModels.Templates
{
    public class GroupViewModel : PropertyChangedBase, IGroup, IData<Group>
    {
        #region Fields
        private Group data;
        #endregion

        #region Properties
        public Group Data => data;

        // TODO: GroupViewModel.cs - validate, can't be null or empty
        public string GroupName
        {
            get => data.Name;
            set
            {
                data.Name = value;
                NotifyOfPropertyChange(() => GroupName);
                NotifyOfPropertyChange(() => Data);
            }
        }
        #endregion

        #region Constructor
        public GroupViewModel()
            : this(new Group(String.Empty)) { }

        public GroupViewModel(Group data)
        {
            this.data = data;
        }
        #endregion
    }
}
