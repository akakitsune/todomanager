﻿using System;

using Caliburn.Micro;

using ToDoManager.Models.Data;

namespace ToDoManager.UserInterface.ViewModels.Templates
{
    public class TaskViewModel : PropertyChangedBase, ITask, IData<Task>
    {
        #region Fields
        private Task data;
        #endregion

        #region Properties
        public Task Data => data;

        // TODO: TaskViewModel.cs - validate, can't be null or empty
        public string TaskTitle
        {
            get => data.Title;
            set
            {
                data.Title = value;
                NotifyOfPropertyChange(() => TaskTitle);
                NotifyOfPropertyChange(() => Data);
            }
        }

        public string TaskDescription
        {
            get => data.Description;
            set
            {
                data.Description = value;
                NotifyOfPropertyChange(() => TaskDescription);
                NotifyOfPropertyChange(() => Data);
            }
        }

        public bool IsTaskDone
        {
            get => data.IsDone;
            set
            {
                data.IsDone = value;
                NotifyOfPropertyChange(() => IsTaskDone);
                NotifyOfPropertyChange(() => Data);
            }
        }

        public Group TaskGroup
        {
            get => data.Group;
            set
            {
                data.Group = value;
                NotifyOfPropertyChange(() => TaskGroup);
                NotifyOfPropertyChange(() => Data);
            }
        }
        #endregion

        #region Constructor
        public TaskViewModel()
            : this(new Task(String.Empty, String.Empty, null)) { }

        public TaskViewModel(Task data)
        {
            this.data = data;
        }
        #endregion
    }
}
