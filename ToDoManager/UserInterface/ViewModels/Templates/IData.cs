﻿namespace ToDoManager.UserInterface.ViewModels.Templates
{
    public interface IData<T>
    {
        T Data { get; }
    }
}
