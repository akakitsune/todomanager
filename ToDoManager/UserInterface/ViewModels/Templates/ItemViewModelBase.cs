﻿using Caliburn.Micro;

namespace ToDoManager.UserInterface.ViewModels.Templates
{
    public abstract class ItemViewModelBase<TItem> : PropertyChangedBase
    {
        protected bool isRemovable = true;
        protected TItem data;

        public TItem Data => data;

        public ItemViewModelBase(TItem data)
        {
            this.data = data;
        }
    }
}
