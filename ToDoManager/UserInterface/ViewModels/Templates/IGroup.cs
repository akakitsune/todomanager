﻿namespace ToDoManager.UserInterface.ViewModels.Templates
{
    public interface IGroup
    {
        string GroupName { get; set; }
    }
}
