﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace ToDoManager.Input
{
    public class KeyTrigger : TriggerBase<UIElement>
    {
        public static readonly DependencyProperty KeyProperty = 
            DependencyProperty.Register("Key", typeof(Key), typeof(KeyTrigger), null);

        public Key Key
        {
            get => (Key) GetValue(KeyProperty);
            set => SetValue(KeyProperty, value);
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.KeyDown += OnAssotiatedObjectKeyDown;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.KeyDown -= OnAssotiatedObjectKeyDown;
        }

        private void OnAssotiatedObjectKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key)
            {
                InvokeActions(e);
            }
        }
    }
}
